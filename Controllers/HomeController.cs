﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recurly;
using Recurly.Constants;
using Recurly.Resources;
using Svp.RecurlyApi.Test.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;

namespace Svp.RecurlyApi.Test.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly Client _client;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            //recurly api key
            _client = new Client(_config["RecurlyApiKey"]);
            //_client = new Client("0f80ed084ee342718b4297b700a5f83c"); // staging
            //_client = new Client("20574be70f59444f932725406f1a3c65"); // Production
        }

        public IActionResult Index()
        {
            //return View();
            return RedirectToAction("BillingInfo");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        public IActionResult BillingInfo()
        {
            var model = new BillingViewModel()
            {
                AccountId = "16298296",
                PlanCode = "620151096_se",
                Email = "test6@sunil.com",
                FirstName = "Sunil Kumar",
                LastName = "Amanchi",
                AddressLine1 = "cirkelv 32",
                AddressLine2 = "",
                City = "jönköping",
                Country = "SE",
                State = "Småland",
                Zip = "55625",
                RecurlyPrivateKey = _config["RecurlyPrivateKey"]
            };
            return View(model);
        }
        [HttpPost]
        public IActionResult AddCardDetails(BillingViewModel model)
        {
            return View(model);
        }
        [HttpPost]
        public IActionResult SubscribeToPlan(BillingViewModel billingViewModel)
        {
            try
            {
                //Create a subscription in recurly
                var subscriptionPurchase = new SubscriptionPurchase()
                {
                    PlanCode = billingViewModel.PlanCode
                };
                var purchaseReq = new PurchaseCreate()
                {
                    Currency = billingViewModel.Currency,
                    Account = new AccountPurchase()
                    {
                        Code = billingViewModel.AccountId,
                        FirstName = billingViewModel.FirstName,
                        LastName = billingViewModel.LastName,
                        Email = billingViewModel.Email
                    },
                    CollectionMethod = CollectionMethod.Automatic,
                    Subscriptions = new List<SubscriptionPurchase>()
                    {
                        subscriptionPurchase
                    }
                };
                var billingInfo = new BillingInfoCreate()
                {
                    TokenId = billingViewModel.Token
                };

                if (!string.IsNullOrEmpty(billingViewModel.ThreeDSecureActionResultTokenId))
                    billingInfo.ThreeDSecureActionResultTokenId = billingViewModel.ThreeDSecureActionResultTokenId;

                purchaseReq.Account.BillingInfo = billingInfo;
                purchaseReq.Account.PreferredLocale = PreferredLocale.EnUs;
                InvoiceCollection collection = _client.CreatePurchase(purchaseReq);
                return Ok("Successfully created the subscription");
            }
            catch (Recurly.Errors.ApiError ex)
            {
                //if the card is needs 3-d secure authentication, it will raise error with code "three_d_secure_action_required" accompanied by "three_d_secure_action_token_id" 
                if (ex.Message.Contains("3-D"))
                {
                    var threeDSecureAuthenticationRequest = new ThreeDSecureAuthentication
                    {
                        ThreeDSecureActionTokenId = ex.Error.TransactionError.ThreeDSecureActionTokenId,
                        ThreeDSecureErrorCode = ex.Error.TransactionError.Code.ToString()
                    };
                    return BadRequest(threeDSecureAuthenticationRequest);
                }
                return BadRequest(ex);
                //throw ex;
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }            
        }
    }
}
