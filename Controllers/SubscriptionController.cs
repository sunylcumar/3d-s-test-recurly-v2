﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Recurly;
using Svp.RecurlyApi.Test.Web.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace Svp.RecurlyApi.Test.Web.Controllers
{
    public class SubscriptionController : Controller
    {
        private readonly Client _client;
        private readonly ILogger<SubscriptionController> _logger;
        private readonly string _mysewnetApiUrl;
        public SubscriptionController(ILogger<SubscriptionController> logger, IConfiguration config)
        {
            _logger = logger;
            //recurly api key
            _client = new Client(config["RecurlyApiKey"]);
            _mysewnetApiUrl = config["MysewnetApiUrl"];
        }
        public IActionResult Index()
        {
            return View();
            //return RedirectToAction("BillingInfo");
        }
        public IActionResult Silver()
        {
            SubscriptionViewModel model = new()
            {
                PlanCode = "620151096",
                PlanDescription = "mysewnet_silver_subscription",
                Amount = 24.99M,
                CompanyCode = "SVPSEWINGBRANDSLLC",
                CurrencyCode = "USD",
                TaxCode = "DC020502",
                AddressLine2 = "",
                AccountId = "4120257",
                AddressLine1 = "141 W Jackson Blvd",
                City = "Chicago",
                Zip = "60604",
                State = "IL",
                Country = "US",
                Email = "sunila@tadigital.com"
            };
            return View(model);
        }
        public IActionResult Platinum()
        {
            SubscriptionViewModel model = new()
            {
                PlanCode = "620151296",
                PlanDescription = "mysewnet_platinum_subscription",
                Amount = 49.99M,
                CompanyCode = "SVPSEWINGBRANDSLLC",
                CurrencyCode = "USD",
                TaxCode = "DC020502",
                AddressLine2 = "",
                AccountId = "4120257",
                AddressLine1 = "141 W Jackson Blvd",
                City = "Chicago",
                Zip = "60604",
                State = "IL",
                Country = "US",
                Email = "sunila@tadigital.com"
            };
            return View(model);
        }
        public IActionResult AddCardDetails(SubscriptionViewModel model)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string url = $"{_mysewnetApiUrl}/Migration/CreateTransaction?taxCode={model.TaxCode}&itemCode={model.PlanCode}&description={model.PlanDescription}&amount={model.Amount}&address1={model.AddressLine1}&address2={model.AddressLine2}&city={model.City}&region={model.State}&postalCode={model.Zip}&country={model.Country}&companyCode={model.CompanyCode}&customerCode={model.AccountId}&currencyCode={model.CurrencyCode}&email={model.Email}";
                    var responseMessage = httpClient.GetAsync(new Uri(url)).Result;
                    var responseContent = responseMessage.Content.ReadAsStringAsync().Result;
                    if (!responseMessage.IsSuccessStatusCode)
                        throw new Exception("Failed to create order in Avalara");

                    return Ok(responseContent);
                }
            }
            catch (Exception e)
            {

            }

            return Ok();
            //return RedirectToAction("BillingInfo");
        }
    }
}
