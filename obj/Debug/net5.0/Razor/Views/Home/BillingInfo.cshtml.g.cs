#pragma checksum "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "46a5aede3fb46a7a2e3c3440e9372ff0cd6de571"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_BillingInfo), @"mvc.1.0.view", @"/Views/Home/BillingInfo.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\_ViewImports.cshtml"
using Svp.RecurlyApi.Test.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\_ViewImports.cshtml"
using Svp.RecurlyApi.Test.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"46a5aede3fb46a7a2e3c3440e9372ff0cd6de571", @"/Views/Home/BillingInfo.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cef6acb98b2d309452b72597642333c847ee1caf", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_BillingInfo : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<BillingViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
  
    ViewData["Title"] = "Billing Info";

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"text-center\">\r\n    <h1 class=\"display-4\">Billing Info</h1>\r\n</div>\r\n");
#nullable restore
#line 8 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
 using (Html.BeginForm("AddCardDetails", "Home", FormMethod.Post, new { @id = "AddBillingAddressForm", role = "form" }))
{

    

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
Write(Html.HiddenFor(m => m.PlanCode));

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
Write(Html.HiddenFor(m => m.AddressLine2));

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
Write(Html.HiddenFor(m => m.RecurlyPrivateKey));

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 15 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.AccountId, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 16 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.AccountId, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 19 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.FirstName, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 20 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.FirstName, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 23 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.LastName, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 24 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.LastName, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 27 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.Email, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 28 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.Email, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 31 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.AddressLine1, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 32 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.AddressLine1, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 35 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.City, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 36 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.City, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 39 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.State, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 40 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.State, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 43 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.Zip, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 44 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.Zip, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        ");
#nullable restore
#line 47 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.LabelFor(m => m.Country, new { @class = "form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 48 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
   Write(Html.TextBoxFor(m => m.Country, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"mb-3\">\r\n        <button type=\"submit\" id=\"submit\" name=\"submit\" class=\"btn btn-primary mb-3\">Submit</button>\r\n    </div>\r\n");
#nullable restore
#line 53 "D:\Projects\svp-consumer-portal\src\Svp.Recurly.Test.Web\Views\Home\BillingInfo.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<BillingViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
