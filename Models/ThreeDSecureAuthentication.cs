﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Svp.RecurlyApi.Test.Web.Models
{
    public class ThreeDSecureAuthentication
    {
        public string ThreeDSecureErrorCode { get; set; }
        public string ThreeDSecureActionTokenId { get; set; }
    }
}
