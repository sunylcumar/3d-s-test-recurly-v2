﻿using System.ComponentModel.DataAnnotations;

namespace Svp.RecurlyApi.Test.Web.Models
{
    public class BillingViewModel
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is a required field.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is a required field.")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is a required field.")]
        public string Email { get; set; }

        [Display(Name = "Address Line 1")]
        [Required(ErrorMessage = "Address Line 1 is a required field.")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City is a required field.")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "State is a required field.")]
        public string State { get; set; }

        [Display(Name = "Zip")]
        [Required(ErrorMessage = "Zip is a required field.")]
        public string Zip { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Country is a required field.")]
        public string Country { get; set; }

        [Display(Name = "AccountId")]
        [Required(ErrorMessage = "AccountId is a required field.")]
        public string AccountId { get; set; }

        [Display(Name = "Plan Code")]
        [Required(ErrorMessage = "Plan Code is a required field.")]
        public string PlanCode { get; set; }

        [Display(Name = "Token")]
        public string Token { get; set; }

        [Display(Name = "3-D Secure Token")]
        public string ThreeDSecureActionResultTokenId { get; set; }

        [Display(Name = "Currency")]
        public string Currency { get; set; }

        [Display(Name = "Recurly Private Key")]
        public string RecurlyPrivateKey { get; set; }
    }
}
