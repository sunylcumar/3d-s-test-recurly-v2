﻿
using System.ComponentModel.DataAnnotations;

namespace Svp.RecurlyApi.Test.Web.Models
{
    public class SubscriptionViewModel
    {
        public string PlanCode { get; set; }
        public string PlanDescription { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string TaxCode { get; set; }
        public string CompanyCode { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is a required field.")]
        public string Email { get; set; }

        [Display(Name = "Address Line 1")]
        [Required(ErrorMessage = "Address Line 1 is a required field.")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City is a required field.")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "State is a required field.")]
        public string State { get; set; }

        [Display(Name = "Zip")]
        [Required(ErrorMessage = "Zip is a required field.")]
        public string Zip { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Country is a required field.")]
        public string Country { get; set; }

        [Display(Name = "AccountId")]
        [Required(ErrorMessage = "AccountId is a required field.")]
        public string AccountId { get; set; }
    }
}
